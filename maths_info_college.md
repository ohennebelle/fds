# Les mathématiques sont partout !? [^1]

### De l'observation de la nature à la nécessité de l'usage des algorithmes

#### >> Observation de la nature

<img src="./images/saturne.jpg" style="width:450px" title="Photo prise par la sonde européenne Cassini, 2007">

> 1) Reconnaissez-vous la planète dont une photo est donnée ci-dessus ?

Approchons nous de son pôle Nord.

<img src="./images/saturne_pole.jpg" style="width:450px" title="Pôle Nord de Saturne, NASA, 2016">

> 2) Que peut-on remarquer ?

On peut retrouver des formes géométriques dans la nature sur Terre.

<img src="./images/alveoles.jpg" style="width:450px;" title="Alvéoles à miel dans une ruche">

**Pourquoi les mathématiques semblent être partout ?**

**Les abeilles font-elles des mathématiques ?**

#### >> Intérêt des algorithmes

> 3) Donner des instructions afin de tracer un carré de coté 1 cm avec l'usage d'un crayon, d'une règle et d'un rapporteur.
> 4) Combien de lignes compte votre programme de construction ?

La Nature cherche à minimiser les instructions pour réduire les données à
mémoriser (dans l'ADN par exemple). 

> 5) Comment construire ce même carré avec un minimum d'instructions ?

*Réponse : usage d'une boucle pour répéter un nombre de fois défini*

```
Répèter 4 fois:
    Avancer de 1 cm
    Tourner de 90° vers la gauche
```

**Programmons le sur un ordinateur avec un langage informatique que vous utiliserez dans un avenir proche: Python**

> 6) Connectez-vous avec les codes fournis.
> 7) Créez un nouveau dossier portant vos prénom et nom.
> 8) Lancez le logiciel Thonny (éditeur de textes pour écrire des programmes en Python).
> 9) Copiez le script ci-dessous et coller le dans Thonny.

```python
from turtle import *  # pour pouvoir réaliser des constructions géométriques
 
# on définit la contruction d'un carré dont la longueur du côté et sa couleur 
# sont données
def carre(longueur, couleur):
    width(4)  # on choisit une épaisseur de 4px pour le crayon
    color(couleur)  # on choisit la couleur du crayon
    begin_fill()  # pour remplir la surface
    for k in range(4):  # on répète 4 fois les instructions suivantes
        forward(longueur)  # on avance de la longueur choisie
        left(90)  # on tourne à gauche d'un angle de l'angle précisé    
    end_fill()
 
carre(100, "red")  # on trace un carré de 100 px de côté de couleur rouge

hideturtle()  # on cache la tortue

getcanvas().postscript(file = "carre.eps")

exitonclick() # on clique sur l'image produite pour fermer la fenêtre
```

> 10) Sauvegardez et exécuter le script ainsi créé dans votre dossier. Appuyez sur la touche $\texttt{F5}$ et nommez le `carre.py` par exemple <img src="./images/carre.jpg" style="width:75px;">.
> 11) Créez un nouveau script afin de construire un hexagone régulier de 100 px de côté de couleur orange. *Vous pourrez nommer le fichier `hexagone.py` et l'image `hexagone.eps`* <img src="./images/hexagone.jpg" style="width:75px;">.


Les principales fonctions du module turtle:

| Fonction                      | Action                                                       |
| ----------------------------- | ------------------------------------------------------------ |
| `goto(x,y)`                   | Aller à l'endroit de coordonnées `x` et `y`                  |
| `forward(distance)`           | Avancer d'une `distance` donnée                              |
| `backward(distance)`          | Reculer d'une `distance` donnée                              |
| `circle(rayon, angle)`        | Tracer un arc de cercle de `rayon` donnée et d'`angle` donné |
| `left(angle)`                 | Tourner à gauche d'un `angle` donné                          |
| `right(angle)`                | Tourner à droite d'un `angle` donné                          |
| `up()`                        | Lever le crayon                                              |
| `down()`                      | Baisser le crayon                                            |
| `color("couleur_choisie")`    | Passer à la couleur `couleur_choisie`                        |
| `width(epaisseur)`            | Sélectionner une `épaisseur` du trait                        |
| `begin_fill()`...`end_fill()` | Remplir la zone de la couleur en cours                       |
| `bgcolor("couleur_choisie")`  | Sélectionner la couleur de l'arrière plan                    |


#### À vous de jouer !

> 12) Réalisez la figure ci-dessous.
> <img src="./images/disques.gif" style="width:150px;">


> 13) Place à votre imagination ! Créez votre propre figure.
> *Si vous manquez d'inspiration, voici quelques idées réalisées par d'autres élèves en seconde.*
> <img src="./images/ruches.jpg" style="width:200px;"> <img src="./images/etoile.jpg" style="width:200px;"> <img src="./images/etoiles.jpg" style="width:200px;"> <img src="./images/astres.jpg" style="width:200px;"> <img src="./images/fleur.jpg" style="width:125px;">

[^1]: Pour les curieux: [documentaire ARTE sur ce thème](https://www.arte.tv/fr/videos/061655-000-A/le-grand-mystere-des-mathematiques/)

