from turtle import *  # pour pouvoir réaliser des constructions géométriques
 
bgcolor("grey")
color("white")
begin_fill()
circle(100, 360)
end_fill()

color("red")
begin_fill()
circle(-150, 360)
end_fill()
up()


goto(0,200)
down()
color("blue")
begin_fill()
circle(50, 360)
end_fill()

hideturtle()  # on cache la tortue

getcanvas().postscript(file = "cercles2.eps")

exitonclick() # on clique sur l'image produite pour fermer la fenêtre