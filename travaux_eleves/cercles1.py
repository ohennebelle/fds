from turtle import *  # pour pouvoir réaliser des constructions géométriques

bgcolor("grey")

color ("white")

begin_fill()
circle(100,360)
end_fill()

color("pink")
begin_fill()
circle(-150,360)
end_fill()

up()
goto(00,200)
down()
begin_fill()
color("yellow")
circle(50,360)
end_fill()

hideturtle()  # on cache la tortue

getcanvas().postscript(file = "cercles.eps")

exitonclick() # on clique sur l'image produite pour fermer la fenêtre
