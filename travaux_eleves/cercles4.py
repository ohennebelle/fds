from turtle import *  # pour pouvoir réaliser des constructions géométriques

bgcolor("grey")

color("yellow")
begin_fill()
circle(50, 360)
end_fill()

color("pink")
begin_fill()
circle(-100,360)
end_fill()

up()
goto(0,100)
color("blue")
begin_fill()
circle(25,360)
end_fill()

hideturtle()  # on cache la tortue

getcanvas().postscript(file = "cercles4.eps")

exitonclick() # on clique sur l'image produite pour fermer la f