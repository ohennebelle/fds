from turtle import *  # pour pouvoir réaliser des constructions géométriques
 
# on définit la contruction d'un carré dont la longueur du côté et sa couleur 
# sont données
def carre(longueur, couleur):
    width(4)  # on choisit une épaisseur de 4px pour le crayon
    color(couleur)  # on choisit la couleur du crayon
    begin_fill()  # pour remplir la surface
    for k in range(4):  # on répète 4 fois les instructions suivantes
        forward(longueur)  # on avance de la longueur choisie
        left(90)  # on tourne à gauche d'un angle de l'angle précisé    
    end_fill()
 
carre(100, "brown")  # on trace un carré de 100 px de côté de couleur rouge

hideturtle()  # on cache la tortue

getcanvas().postscript(file = "carre.eps")

exitonclick() # on clique sur l'image produite pour fermer la fenêtre