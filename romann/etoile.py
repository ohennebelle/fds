from turtle import *  # pour pouvoir réaliser des constructions géométriques
 
# on définit la contruction d'un carré dont la longueur du côté et sa couleur 
# sont données
def etoile(longueur, couleur):
    width(8)  # on choisit une épaisseur de 4px pour le crayon
    color(couleur)  # on choisit la couleur du crayon
    begin_fill()  # pour remplir la surface
    for k in range(8):  # on répète 4 fois les instructions suivantes
        forward(longueur)  # on avance de la longueur choisie
        left(135)  # on tourne à gauche d'un angle de l'angle précisé    
    end_fill()
 
etoile(135, "green")  # on trace un carré de 100 px de côté de couleur rouge
up()
goto(100, 200)
down()
etoile(200, "purple")
circle(100,200)

hideturtle()  # on cache la tortue

getcanvas().postscript(file = "etoile.eps")

exitonclick() # on clique sur l'image produite pour fermer la fenêtre


